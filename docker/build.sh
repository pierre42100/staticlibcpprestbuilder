#!/bin/bash

CONTAINER_IMAGE=buildlibcpprest
CONTAINER_NAME=tmp_image

docker build . -t $CONTAINER_IMAGE

docker run --name=$CONTAINER_NAME $CONTAINER_IMAGE

docker cp $CONTAINER_NAME:/casablanca/build.release/Release/Binaries output

docker container rm $CONTAINER_NAME
